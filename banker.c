#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include "banker.h"
//#include "message.h"


message_t get_message()
{

}

void handle_message(message_t *msg)
{

}

int main()
{
    // Variable declaration
    int queue_id;
    int msg_stats;
    key_t queue_key;
    msgBuf msg_buf;
    message_t *msg;

    // Open the file 
    FILE *in;
    in = fopen("initial.data", 'r');
    
    // Initialize the parameters from the file
    banker_params_t params;

    fscanf(in, "%d", &params.num_resource_types);
    params->resource_types = malloc(params.num_resource_types * sizeof(int));
    if (!params.resource_types)
    {
        printf("Memory allocation failed! Exit.\n");
        exit(-1);
    }

    for (int i = 0; i < params.num_resource_types, i++)
    {
        fscanf(in, "%d", params->resource_types[i]);
    }
    
    // Initialize static upper limit of resource types
    limit_resource_types = params.num_resource_types;
    
    // Create message queue
    queue_key = "BANKER";
    queue_id = msgget(queue_key, IPC_EXCL | 0600);
    if (queue_id < 0)
    {
        printf("Mailbox creation failed\n")
        exit(-2);
    }
    else
    {
        printf("The Banker process created a mailbox with ID: %d\n", queue_id);
    }
    
    // Wait indefinitely for incoming messages
    while (true)
    {
        msg_stats = msgrcv(queue_id, &msg_buf, MSG_INFO_SIZE, 0, 0);
        if (msg_stats < 0)
        {
            printf("Message reciept failed!\n");
            exit(-2);
        }
        else
        {
            msg = &msg_buf;
        }
            
    }
    
}
