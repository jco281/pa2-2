#ifndef banker_h
#define banker_h

typedef struct {
    int num_clients;
    int num_resource_types;
    int *resource_types;
}banker_params_t;

static int limit_resource_types;
static int limit_num_clients;

#endif
