all: banker.o client.o
    gcc main.c banker.o client.o -std=c99 -o main

banker.o: banker.c banker.h client.h
    gcc banker.c -std=c99 -o banker.o

client.o: client.c banker.h client.h
    gcc client.c -std=c99 -o client.o

clean:
    rm *.o; rm driver
